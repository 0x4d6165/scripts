#!/bin/sh

mbsync -a

# notmuch new
# notmuch tag +inbox +unread -new -- tag:new

# notmuch tag +debian +debian-announce -inbox cc:debian-devel-announce-requests@lists.debian.org or to:debian-devel-announce-requests@lists.debian.org
# notmuch tag +debian +debian-mentors -inbox cc:debian-mentors@lists.debian.org or to:debian-mentors@lists.debian.org
# notmuch tag +debian +debian-bugs -inbox cc:debian-bugs@lists.debian.org or to:debian-bugs@lists.debian.org
# notmuch tag -inbox +archive "folder:Archive"
# notmuch tag +sent -new -inbox -unread from:maeborow@posteo.net 
# notmuch tag +sent -new -inbox -unread tag:replied

# notify-send "Mail synced $(notmuch search tag:unread | wc -l) unread messages"
mu index
