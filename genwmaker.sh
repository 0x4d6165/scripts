#!/bin/sh

. "$HOME/.cache/wal/colors.sh"

echo "{
TitleJustify = left;
HighlightColor = \"${color0}\";
HighlightTextColor = \"${color10}\";
ClipTitleColor = \"${color0}\";
CClipTitleColor = gray20;
FTitleColor = \"${color0}\";
PTitleColor = \"${color7}\";
UTitleColor = \"${color7}\";
FTitleBack = (solid, \"${color5}\");
PTitleBack = (solid, \"${color2}\");
UTitleBack = (solid, \"${color2}\");
ResizebarBack = (solid, \"${color2}\");
MenuTitleColor = \"${color0}\";
MenuTextColor = \"${color0}\";
MenuDisabledColor = gray40;
MenuTitleBack = (solid, \"${color5}\");
MenuTextBack = (solid, \"${color10}\");
IconBack = (solid, \"${color10}\");
IconTitleColor = \"${color10}\";
IconTitleBack = \"${color0}\";
MenuStyle = flat;
}
">"$HOME"/GNUstep/Library/WindowMaker/Styles/Wal.style

setstyle "$HOME"/GNUstep/Library/WindowMaker/Styles/Wal.style
