#!/bin/sh

mkdir -p ~/.build
cd ~/.build || exit

git clone --depth 1 --branch emacs-29 https://git.savannah.gnu.org/git/emacs.git
cd emacs || exit

sudo apt build-dep emacs

./autogen.sh
./configure --with-pgtk --with-json --with-tree-sitter --with-imagemagick --with-native-compilation
make -j8
