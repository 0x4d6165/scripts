#!/bin/bash

set -e

if [[ $# -ne 4 ]]; then
    echo "usage: $0 <video filename> <start time in seconds> <duration in seconds> <output gif filename>" >&2
    exit 1
fi

urlregex='(https?|ftp|file)://[-[:alnum:]\+&@#/%?=~_|!:,.;]*[-[:alnum:]\+&@#/%=~_|]'

if [[ "$1" =~ $urlregex ]]
then
    yt-dlp "$1" -f "bv*[ext=mp4]+ba[ext=vorbis]/b[ext=mp4] / bv*+ba/b" --remux-video mkv -o clip.mkv
    ffmpeg -ss "$2" -t "$3" -i clip.mkv -filter_complex "[0:v] split [a][b];[a] palettegen [p];[b][p] paletteuse" "$4"
    gifsicle -O3 --lossy=75 "$4" -o "$4"
else
    ffmpeg -ss "$2" -t "$3" -i "$1" -filter_complex "[0:v] split [a][b];[a] palettegen [p];[b][p] paletteuse" "$4"
    gifsicle -O3 --lossy=75 "$4" -o "$4"
fi
